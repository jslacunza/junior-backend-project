# Junior Back End Developer [![coverage](https://gitlab.com/jslacunza/junior-backend-project/badges/main/coverage.svg)](https://gitlab.com/jslacunza/junior-backend-project) [![pipeline](https://gitlab.com/jslacunza/junior-backend-project/badges/main/pipeline.svg)](https://gitlab.com/jslacunza/junior-backend-project)
## Hacker News

Web app based on NodeJS, NestJS, MongoDB and ReactJS that call HN API, filter and save the data into DB

## Installation

Copy this repo
```sh
git clone https://gitlab.com/jslacunza/warmup-project.git
```
Run the composer command to run the project
```sh
docker-compose up
```
Wait for the news to be saved into db
Visit http://localhost to see the app

Hello there!
This is a backend intern project for Reing, here you can read all the documentation for the use of this app.

This project is about a basic API who calls to another API to fetch and filter the data to our own database.

 This app is building with the last release of NestJS and MongoDB, running inside a docker container.

## Installation

You should copy this repository to your local machine with
```
git clone https://gitlab.com/jslacunza/junior-backend-project.git
```
Then cd into the container folder
```
cd ./container-folder
```
Run docker compose command
```
docker-compose build
docker-compose up
```
 **You should wait to docker-compose finish to runnit before doing the tests**
 
## Endpoints
Inside this repository are a Postman file to test the API you should [download it here](https://gitlab.com/jslacunza/junior-backend-project/-/blob/main/hn-postman.postman_collection.json) and import it to your Postman app.

**Root /**

This project don't have a / directory, if you try to connect to you'll recibe a 404 error

**Get news**

To get all the news you should send a **GET** request to this url, te response should be an array of news
```
http://localhost:3000/api/news
```
All responeses on this endpoint are paginated by 5 news, if you want the next page, you should use the page key inside the body of your request
|Key| Value |
|--|--|
| page | 2|

To filter the search you can use author, tags or title to make your search
```
http://localhost:3000/api/news?tags=comment
http://localhost:3000/api/news?author=throwaway894345
http://localhost:3000/api/news?title=node
```

**Delete news**

To delete news you should make a **DELETE** request to the endpoint and give the id of the news you want to delete
```
http://localhost:3000/api/news/62025442e61eae3da4f03f58
```