FROM node:16.13.2-alpine3.14 as build

WORKDIR /app
COPY . .
RUN npm install
RUN npm run build

FROM node:16.13.2-alpine3.14 as env
WORKDIR /app
COPY --from=build /app/dist ./dist
COPY --from=build /app/package.json ./

RUN npm install --prod

EXPOSE 3000
CMD [ "npm", "run",  "start:prod" ]
