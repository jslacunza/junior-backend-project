import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ScheduleModule } from '@nestjs/schedule';
import { JobsModule } from './jobs/jobs.module';
import { NewsModule } from './news/news.module';
import config from './configs/keys';
import { WinstonModule } from 'nest-winston';
@Module({
  imports: [
    MongooseModule.forRoot(process.env.MONGO_URL || config.mongoURI),
    WinstonModule.forRoot({}),
    ScheduleModule.forRoot(),
    NewsModule,
    JobsModule,
  ],
})
export class AppModule {}
