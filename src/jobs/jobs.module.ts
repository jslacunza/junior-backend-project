import { Module } from '@nestjs/common';
import { NewsModule } from '../news/news.module';
import { JobsService } from './jobs.service';

@Module({
  imports: [NewsModule],
  providers: [JobsService],
})
export class JobsModule {}
