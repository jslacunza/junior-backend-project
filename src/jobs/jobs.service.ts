import { Injectable } from '@nestjs/common';
import { Cron, Timeout } from '@nestjs/schedule';
import { NewsService } from 'src/news/news.service';

const url = 'https://hn.algolia.com/api/v1/search_by_date?query=nodejs';

@Injectable()
export class JobsService {
  constructor(private readonly newsService: NewsService) {}

  @Timeout(1000)
  async initialFetch() {
    await this.newsService.getNewsAndSave(url);
  }

  @Cron('0 0 */1 * * *')
  async recurrentFetch() {
    await this.newsService.getNewsAndSave(url);
  }
}
