import { IsNotEmpty } from 'class-validator';

export class CreateNewsDto {
  @IsNotEmpty()
  title: string;
  @IsNotEmpty()
  author: string;
  @IsNotEmpty()
  url: string;
  @IsNotEmpty()
  createdAt: string;
  @IsNotEmpty()
  tags: string[];
  @IsNotEmpty()
  objectID: string;
}
