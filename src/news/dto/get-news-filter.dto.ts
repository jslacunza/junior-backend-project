export class GetNewsFilterDto {
  author: string;
  title: string;
  tags: string[];
}
