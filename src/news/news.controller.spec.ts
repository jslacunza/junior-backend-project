import { Test, TestingModule } from '@nestjs/testing';
import { GetNewsFilterDto } from './dto/get-news-filter.dto';
import { NewsController } from './news.controller';
import { NewsService } from './news.service';

describe('NewsController', () => {
  let controller: NewsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [NewsController],
      providers: [NewsService],
    })
      .overrideProvider(NewsService)
      .useValue(mockNewsService)
      .compile();

    controller = module.get<NewsController>(NewsController);
  });

  describe('getNews()', () => {
    it('should return an arr of news', async () => {
      expect(await controller.getNews(emptyFilter)).toEqual([baseNews]);
    });
    it('should return an arr of news', async () => {
      expect(await controller.getNews(filledFilter)).toEqual([baseNews]);
    });
  });

  describe('deleteNews()', () => {
    it('should return a successul message of deletion', async () => {
      expect(await controller.deleteNews(baseNews._id)).toEqual({
        deleted: true,
        message: `${baseNews._id} was deleted successfully`,
      });
    });
  });
});
// Mock thigs
// Service
const mockNewsService = {
  getAllNews: jest.fn(() => {
    return [baseNews];
  }),
  getNewsWithFilter: jest.fn(() => {
    return [baseNews];
  }),
  deleteNews: jest.fn(() => {
    return {
      deleted: true,
      message: `${baseNews._id} was deleted successfully`,
    };
  }),
};
// Returned news
const baseNews = {
  _id: '62025442e61eae3da4f03f44',
  isDeleted: false,
  objectID: '30257057',
  tags: ['comment', 'author_lxpz', 'story_30256753'],
  createdAt: '2022-02-08T11:03:19.000Z',
  url: 'https://garagehq.deuxfleurs.fr/blog/2022-introducing-garage/',
  author: 'lxpz',
  title: 'Garage, our self-hosted distributed object storage solution',
  __v: 0,
};
// Empty filter
const emptyFilter = <GetNewsFilterDto>{};
const filledFilter = <GetNewsFilterDto>{ tags: ['comment'] };
