import { Body, Controller, Delete, Get, Param, Query } from '@nestjs/common';
import { GetNewsFilterDto } from './dto/get-news-filter.dto';
import { NewsService } from './news.service';
import { News } from './schemas/news.schema';

@Controller('news')
export class NewsController {
  constructor(private readonly newsService: NewsService) {}

  @Get()
  getNews(
    @Query() filterDto: GetNewsFilterDto,
    @Body('page') page?: number,
  ): Promise<News[]> {
    if (Object.keys(filterDto).length) {
      return this.newsService.getNewsWithFilter(filterDto, page);
    } else {
      return this.newsService.getAllNews(page);
    }
  }

  @Delete('/:id')
  deleteNews(
    @Param('id') id: string,
  ): Promise<{ deleted: boolean; message?: string }> {
    return this.newsService.deleteNews(id);
  }
}
