import { getModelToken, MongooseModule } from '@nestjs/mongoose';
import { Test, TestingModule } from '@nestjs/testing';
import { WINSTON_MODULE_NEST_PROVIDER } from 'nest-winston';
import { NewsService } from './news.service';
import { NewsRepository } from './repository/news.repository';
import { News, NewsSchema } from './schemas/news.schema';
import { NotFoundException } from '@nestjs/common';
import {
  closeInMongodConnection,
  rootMongooseTestModule,
} from '../../test/test-utils/mongooseTestCon';
import { GetNewsFilterDto } from './dto/get-news-filter.dto';

jest.mock('winston');

const mockModel = {};

const toBeCreatedNews = {
  title: 'A new',
  author: 'test',
  url: 'https://qwer.com',
  createdAt: 'time',
  tags: ['test', 'tags'],
  objectID: '1234',
};

const createdNews = {
  title: 'A new',
  author: 'test',
  url: 'https://qwer.com',
  createdAt: 'time',
  tags: ['test', 'tags'],
  objectID: '1234',
  isDeleted: false,
  _id: 'asdf',
  __v: 0,
};
const deletedNews = {
  title: 'A new',
  author: 'test',
  url: 'https://qwer.com',
  createdAt: 'time',
  tags: ['test', 'tags'],
  objectID: '1234',
  isDeleted: true,
  _id: 'asdf',
  __v: 0,
};

const authorFilter = <GetNewsFilterDto>{ author: 'test' };
const titleFilter = <GetNewsFilterDto>{ title: 'test' };
const tagsFilter = <GetNewsFilterDto>{ tags: ['test'] };

const deletedMessage = {
  deleted: true,
  message: `News ${createdNews._id} was successfully delete`,
};

describe('NewsService', () => {
  let service: NewsService;
  let repository: NewsRepository;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        NewsService,
        NewsRepository,
        {
          provide: WINSTON_MODULE_NEST_PROVIDER,
          useValue: {
            log: jest.fn(),
          },
        },
        {
          provide: getModelToken('News'),
          useValue: mockModel,
        },
      ],
      imports: [
        rootMongooseTestModule(),
        MongooseModule.forFeature([{ name: News.name, schema: NewsSchema }]),
      ],
    }).compile();

    service = module.get<NewsService>(NewsService);
    repository = module.get<NewsRepository>(NewsRepository);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('createNews()', () => {
    it('should return DB saved News', async () => {
      repository.createNews = jest.fn().mockReturnValueOnce(createdNews);
      const result = await service.createNews(toBeCreatedNews);

      expect(result).toBeDefined();
      expect(result).toMatchObject(createdNews);
      expect(repository.createNews).toBeCalled();
    });

    it('should return null because news exist', async () => {
      repository.createNews = jest.fn().mockReturnValueOnce(null);
      const result = await service.createNews(toBeCreatedNews);

      expect(result).toBeDefined();
      expect(result).toEqual(null);
      expect(repository.createNews).toBeCalled();
    });
  });

  describe('getAllNews()', () => {
    it('should return an arr of news, no page gived', async () => {
      repository.getNews = jest.fn().mockReturnValueOnce([createdNews]);
      const result = await service.getAllNews();

      expect(result).toBeDefined();
      expect(result).toMatchObject([createdNews]);
      expect(repository.getNews).toBeCalled();
    });

    it('should return an arr of news, page 0 gived', async () => {
      repository.getNews = jest.fn().mockReturnValueOnce([createdNews]);
      const result = await service.getAllNews(0);

      expect(result).toBeDefined();
      expect(result).toMatchObject([createdNews]);
      expect(repository.getNews).toBeCalled();
    });

    it('should return an empty arr, page over qt gived', async () => {
      repository.getNews = jest.fn().mockReturnValueOnce([]);
      const result = await service.getAllNews(1);

      expect(result).toBeDefined();
      expect(result).toMatchObject([]);
      expect(repository.getNews).toBeCalled();
    });
  });

  describe('getNewsWithFilter', () => {
    describe('test with author filter', () => {
      it('no page given, should return news arr', async () => {
        repository.getNewsByAuthor = jest
          .fn()
          .mockReturnValueOnce([createdNews]);
        repository.getNewsByTitle = jest.fn().mockReturnValueOnce([]);
        repository.getNewsByTags = jest.fn().mockReturnValueOnce([]);
        const result = await service.getNewsWithFilter(authorFilter);

        expect(result).toBeDefined();
        expect(result).toMatchObject([createdNews]);
        expect(repository.getNewsByAuthor).toBeCalled();
        expect(repository.getNewsByTitle).not.toBeCalled();
        expect(repository.getNewsByTags).not.toBeCalled();
      });
      it('page 0 given, should return news arr', async () => {
        repository.getNewsByAuthor = jest
          .fn()
          .mockReturnValueOnce([createdNews]);
        repository.getNewsByTitle = jest.fn().mockReturnValueOnce([]);
        repository.getNewsByTags = jest.fn().mockReturnValueOnce([]);
        const result = await service.getNewsWithFilter(authorFilter, 0);

        expect(result).toBeDefined();
        expect(result).toMatchObject([createdNews]);
        expect(repository.getNewsByAuthor).toBeCalled();
        expect(repository.getNewsByTitle).not.toBeCalled();
        expect(repository.getNewsByTags).not.toBeCalled();
      });
      it('page 2 given, should return an empty arr', async () => {
        repository.getNewsByAuthor = jest.fn().mockReturnValueOnce([]);
        repository.getNewsByTitle = jest.fn().mockReturnValueOnce([]);
        repository.getNewsByTags = jest.fn().mockReturnValueOnce([]);
        const result = await service.getNewsWithFilter(authorFilter, 1);

        expect(result).toBeDefined();
        expect(result).toMatchObject([]);
        expect(repository.getNewsByAuthor).toBeCalled();
        expect(repository.getNewsByTitle).not.toBeCalled();
        expect(repository.getNewsByTags).not.toBeCalled();
      });
    });
    describe('test with title filter', () => {
      it('no page given, should return news arr', async () => {
        repository.getNewsByTitle = jest
          .fn()
          .mockReturnValueOnce([createdNews]);
        repository.getNewsByAuthor = jest.fn().mockReturnValueOnce([]);
        repository.getNewsByTags = jest.fn().mockReturnValueOnce([]);

        const result = await service.getNewsWithFilter(titleFilter);

        expect(result).toBeDefined();
        expect(result).toMatchObject([createdNews]);
        expect(repository.getNewsByTitle).toBeCalled();
        expect(repository.getNewsByAuthor).not.toBeCalled();
        expect(repository.getNewsByTags).not.toBeCalled();
      });
      it('page 0 given, should return news arr', async () => {
        repository.getNewsByTitle = jest
          .fn()
          .mockReturnValueOnce([createdNews]);
        repository.getNewsByAuthor = jest.fn().mockReturnValueOnce([]);
        repository.getNewsByTags = jest.fn().mockReturnValueOnce([]);

        const result = await service.getNewsWithFilter(titleFilter, 0);

        expect(result).toBeDefined();
        expect(result).toMatchObject([createdNews]);
        expect(repository.getNewsByTitle).toBeCalled();
        expect(repository.getNewsByAuthor).not.toBeCalled();
        expect(repository.getNewsByTags).not.toBeCalled();
      });
      it('page 2 given, should return an empty arr', async () => {
        repository.getNewsByTitle = jest.fn().mockReturnValueOnce([]);
        repository.getNewsByAuthor = jest.fn().mockReturnValueOnce([]);
        repository.getNewsByTags = jest.fn().mockReturnValueOnce([]);

        const result = await service.getNewsWithFilter(titleFilter, 1);

        expect(result).toBeDefined();
        expect(result).toMatchObject([]);
        expect(repository.getNewsByTitle).toBeCalled();
        expect(repository.getNewsByAuthor).not.toBeCalled();
        expect(repository.getNewsByTags).not.toBeCalled();
      });
    });
    describe('test with tags filter', () => {
      it('no page given, should return news arr', async () => {
        repository.getNewsByTags = jest.fn().mockReturnValueOnce([createdNews]);
        repository.getNewsByTitle = jest.fn().mockReturnValueOnce([]);
        repository.getNewsByAuthor = jest.fn().mockReturnValueOnce([]);

        const result = await service.getNewsWithFilter(tagsFilter);

        expect(result).toBeDefined();
        expect(result).toMatchObject([createdNews]);
        expect(repository.getNewsByTags).toBeCalled();
        expect(repository.getNewsByTitle).not.toBeCalled();
        expect(repository.getNewsByAuthor).not.toBeCalled();
      });
      it('page 0 given, should return news arr', async () => {
        repository.getNewsByTags = jest.fn().mockReturnValueOnce([createdNews]);
        repository.getNewsByTitle = jest.fn().mockReturnValueOnce([]);
        repository.getNewsByAuthor = jest.fn().mockReturnValueOnce([]);

        const result = await service.getNewsWithFilter(tagsFilter, 0);

        expect(result).toBeDefined();
        expect(result).toMatchObject([createdNews]);
        expect(repository.getNewsByTags).toBeCalled();
        expect(repository.getNewsByTitle).not.toBeCalled();
        expect(repository.getNewsByAuthor).not.toBeCalled();
      });
      it('page 2 given, should return an empty arr', async () => {
        repository.getNewsByTags = jest.fn().mockReturnValueOnce([]);
        repository.getNewsByTitle = jest.fn().mockReturnValueOnce([]);
        repository.getNewsByAuthor = jest.fn().mockReturnValueOnce([]);

        const result = await service.getNewsWithFilter(tagsFilter, 1);

        expect(result).toBeDefined();
        expect(result).toMatchObject([]);
        expect(repository.getNewsByTags).toBeCalled();
        expect(repository.getNewsByTitle).not.toBeCalled();
        expect(repository.getNewsByAuthor).not.toBeCalled();
      });
    });
  });

  describe('deleteNews', () => {
    it('should return an obj with a deleted message', async () => {
      repository.getNewsById = jest.fn().mockReturnValueOnce(createdNews);
      repository.deleteNews = jest.fn().mockReturnValueOnce(deletedMessage);
      const result = await service.deleteNews(createdNews._id);

      expect(result).toBeDefined();
      expect(result).toMatchObject(deletedMessage);
      expect(repository.getNewsById).toBeCalled();
      expect(repository.deleteNews).toBeCalled();
    });

    it('should throw an error because the news is already deleted', async () => {
      repository.getNewsById = jest.fn().mockReturnValueOnce(deletedNews);

      try {
        const result = await service.deleteNews(createdNews._id);
        expect(result).toBeDefined();
        expect(repository.getNewsById).toBeCalled();
        expect(repository.deleteNews).not.toBeCalled();
      } catch (error) {
        expect(error).toBeInstanceOf(NotFoundException);
      }
    });

    it('should throw an error because the news was not found', async () => {
      repository.getNewsById = jest.fn().mockReturnValueOnce(null);

      try {
        const result = await service.deleteNews(createdNews._id);
        expect(result).toBeDefined();
        expect(repository.getNewsById).toBeCalled();
        expect(repository.deleteNews).not.toBeCalled();
      } catch (error) {
        expect(error).toBeInstanceOf(NotFoundException);
      }
    })
  });

  afterAll(async () => {
    await closeInMongodConnection();
  });
});
