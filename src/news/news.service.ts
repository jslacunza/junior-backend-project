import {
  Inject,
  Injectable,
  LoggerService,
  NotFoundException,
} from '@nestjs/common';
import axios from 'axios';
import { WINSTON_MODULE_NEST_PROVIDER } from 'nest-winston';
import { Logger } from 'winston';
import { CreateNewsDto } from './dto/create-news.dto';
import { GetNewsFilterDto } from './dto/get-news-filter.dto';
import { NewsRepository } from './repository/news.repository';
import { News } from './schemas/news.schema';

const newsPerPage = 5;

@Injectable()
export class NewsService {
  constructor(
    private readonly newsRepository: NewsRepository,
    @Inject(WINSTON_MODULE_NEST_PROVIDER)
    private readonly logger: LoggerService,
  ) {}

  createNews(createNewsDto: CreateNewsDto): Promise<News> {
    return this.newsRepository.createNews(createNewsDto);
  }

  getAllNews(page?: number): Promise<News[]> {
    let toSkip = 0;
    !page ? toSkip : (toSkip = page * newsPerPage);
    return this.newsRepository.getNews(toSkip, newsPerPage);
  }

  getNewsWithFilter(
    filterDto: GetNewsFilterDto,
    page?: number,
  ): Promise<News[]> {
    const { author, title, tags } = filterDto;

    let toSkip;
    !page ? (toSkip = 0) : (toSkip = page * newsPerPage);

    if (author)
      return this.newsRepository.getNewsByAuthor(author, toSkip, newsPerPage);
    if (title)
      return this.newsRepository.getNewsByTitle(title, toSkip, newsPerPage);
    if (tags)
      return this.newsRepository.getNewsByTags(tags, toSkip, newsPerPage);
  }

  async deleteNews(id: string): Promise<{ deleted: boolean; message: string }> {
    const exist = await this.newsRepository.getNewsById(id);
    if (exist && !exist.isDeleted) {
      await this.newsRepository.deleteNews(id);
      return { deleted: true, message: `News ${id} was successfully delete` };
    } else {
      throw new NotFoundException('News Not found');
    }
  }

  async getNewsAndSave(url: string): Promise<News[]> {
    // Get the response
    const res = await axios.get(url);
    const promiseNews = await Promise.all(
      res.data.hits.map((news) => {
        // Add title or story_title to title
        const title: string = news.title || news.story_title;
        // Add url or story_url to url
        const url: string = news.url || news.story_url;
        // filter news by missing title or url
        if (title && url) {
          const newPost: CreateNewsDto = {
            title: title,
            author: news.author,
            url: url,
            createdAt: news.created_at,
            tags: news._tags,
            objectID: news.objectID,
          };
          // Save filtered news into DB
          return this.createNews(newPost);
        }
      }),
    );
    const uploadedNews = promiseNews.filter(Boolean);
    this.logger.log(`${uploadedNews.length} stories stored into database`);
    return uploadedNews;
  }
}
