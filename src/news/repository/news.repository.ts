import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateNewsDto } from '../dto/create-news.dto';
import { News, NewsDocument } from '../schemas/news.schema';

@Injectable()
export class NewsRepository {
  constructor(
    @InjectModel(News.name) private readonly newsModel: Model<NewsDocument>,
  ) {}

  async createNews(createNewsDto: CreateNewsDto): Promise<News> {
    const exist = await this.checkExist(createNewsDto.objectID);
    const baseNews = {
      ...createNewsDto,
      isDeleted: false,
    };
    if (!exist) return this.newsModel.create(baseNews);
  }

  getNewsById(id: string): Promise<News> {
    return this.newsModel.findById(id).exec();
  }

  getNews(toSkip: number, newsPerPage: number): Promise<News[]> {
    return this.newsModel
      .find({ isDeleted: false })
      .sort({ created_at: -1 })
      .skip(toSkip)
      .limit(newsPerPage)
      .exec();
  }

  getNewsByAuthor(
    author: string,
    toSkip: number,
    newsPerPage: number,
  ): Promise<News[]> {
    return this.newsModel
      .find({ author, isDeleted: false })
      .sort({ created_at: -1 })
      .skip(toSkip)
      .limit(newsPerPage)
      .exec();
  }

  getNewsByTitle(
    title: string,
    toSkip: number,
    newsPerPage: number,
  ): Promise<News[]> {
    return this.newsModel
      .find({ title: { $regex: title }, isDeleted: false })
      .sort({ created_at: -1 })
      .skip(toSkip)
      .limit(newsPerPage)
      .exec();
  }

  getNewsByTags(
    tags: string[],
    toSkip: number,
    newsPerPage: number,
  ): Promise<News[]> {
    return this.newsModel
      .find({
        tags: { $in: tags.toString().toLocaleLowerCase().split(' ') },
        isDeleted: false,
      })
      .sort({ created_at: -1 })
      .skip(toSkip)
      .limit(newsPerPage)
      .exec();
  }

  deleteNews(id: string): Promise<News> {
    return this.newsModel
      .findByIdAndUpdate(id, {
        isDeleted: true,
      })
      .exec();
  }

  checkExist(objectID: string): Promise<News> {
    return this.newsModel.findOne({ objectID: objectID }).exec();
  }
}
