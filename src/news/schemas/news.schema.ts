import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type NewsDocument = News & Document;

@Schema()
export class News {
  @Prop()
  title: string;
  @Prop()
  author: string;
  @Prop()
  url: string;
  @Prop()
  createdAt: string;
  @Prop([String])
  tags: string[];
  @Prop()
  objectID: string;
  @Prop()
  isDeleted: boolean;
}
export const NewsSchema = SchemaFactory.createForClass(News);
