export const foundNews = {
  _id: '61fff30bc17607f336d1e4a5',
  isDeleted: false,
  objectID: '30213770',
  tags: ['comment', 'author_password4321', 'story_30206989'],
  createdAt: '2022-02-04T21:47:35.000Z',
  url: 'https://reviewbunny.app/blog/dont-make-me-think-or-why-i-switched-to-rails-from-javascript-spas',
  author: 'password4321',
  title: "Don't make me think, or why I switched to Rails from JavaScript SPAs",
  __v: 0,
};
export const shortNewsFromDB = [
  {
    objectID: '30251066',
    tags: ['comment', 'author_kashunstva', 'story_30250111'],
    createdAt: '2022-02-07T21:00:31.000Z',
    url: 'https://www.latimes.com/entertainment-arts/business/story/2022-02-06/spotify-joe-rogan-daniel-ek-neil-young',
    author: 'kashunstva',
    title: 'Spotify’s CEO apologizes to employees over Joe Rogan controversy',
  },
];

export const axiosRes = {
  data: {
    hits: [
      {
        created_at: '2022-02-07T21:00:31.000Z',
        title: null,
        url: null,
        author: 'kashunstva',
        points: null,
        story_text: null,
        comment_text:
          'But the 18-20+ group are still nodes in a complex system that includes others who aren’t so healthy. Our friend teaches an intro to sociology course at university. She also has a child with a chronic disease and an elderly parent whom she cares for. Walking into a lecture hall packed with unvaccinated 18+ students is unnerving, not because of their risk which is negligible but because of the downstream risks that they impose on the system. It’s not unreasonable to consider the risk of others with whom one interacts in reasoning through the decision to get vaccinated or not. Now I have no idea what exactly Rogan said but a categorical “No” is not the response I’d expect from someone who has really thought about the issues. But then again the blurring lines between entertainment and information has its risks.',
        num_comments: null,
        story_id: 30250111,
        story_title:
          'Spotify’s CEO apologizes to employees over Joe Rogan controversy',
        story_url:
          'https://www.latimes.com/entertainment-arts/business/story/2022-02-06/spotify-joe-rogan-daniel-ek-neil-young',
        parent_id: 30250747,
        created_at_i: 1644267631,
        _tags: ['comment', 'author_kashunstva', 'story_30250111'],
        objectID: '30251066',
        _highlightResult: {
          author: {
            value: 'kashunstva',
            matchLevel: 'none',
            matchedWords: [],
          },
          comment_text: {
            value:
              'But the 18-20+ group are still <em>nodes</em> in a complex system that includes others who aren’t so healthy. Our friend teaches an intro to sociology course at university. She also has a child with a chronic disease and an elderly parent whom she cares for. Walking into a lecture hall packed with unvaccinated 18+ students is unnerving, not because of their risk which is negligible but because of the downstream risks that they impose on the system. It’s not unreasonable to consider the risk of others with whom one interacts in reasoning through the decision to get vaccinated or not. Now I have no idea what exactly Rogan said but a categorical “No” is not the response I’d expect from someone who has really thought about the issues. But then again the blurring lines between entertainment and information has its risks.',
            matchLevel: 'full',
            fullyHighlighted: false,
            matchedWords: ['nodejs'],
          },
          story_title: {
            value:
              'Spotify’s CEO apologizes to employees over Joe Rogan controversy',
            matchLevel: 'none',
            matchedWords: [],
          },
          story_url: {
            value:
              'https://www.latimes.com/entertainment-arts/business/story/2022-02-06/spotify-joe-rogan-daniel-ek-neil-young',
            matchLevel: 'none',
            matchedWords: [],
          },
        },
      },
    ],
    nbHits: 22234,
    page: 0,
    nbPages: 50,
    hitsPerPage: 20,
    exhaustiveNbHits: true,
    exhaustiveTypo: true,
    query: 'nodejs',
    params:
      'advancedSyntax=true&analytics=true&analyticsTags=backend&query=nodejs',
    processingTimeMS: 8,
  },
};
export const allNewsFromDB = [
  {
    _id: '61fff30bc17607f336d1e4a5',
    isDeleted: false,
    objectID: '30213770',
    tags: ['comment', 'author_password4321', 'story_30206989'],
    createdAt: '2022-02-04T21:47:35.000Z',
    url: 'https://reviewbunny.app/blog/dont-make-me-think-or-why-i-switched-to-rails-from-javascript-spas',
    author: 'password4321',
    title:
      "Don't make me think, or why I switched to Rails from JavaScript SPAs",
    __v: 0,
  },
  {
    _id: '61fff30bc17607f336d1e4a7',
    isDeleted: false,
    objectID: '30212367',
    tags: ['comment', 'author_followtherhythm', 'story_30209706'],
    createdAt: '2022-02-04T20:12:36.000Z',
    url: 'https://redpanda.com/blog/data-transformation-engine-with-wasm-runtime/',
    author: 'followtherhythm',
    title: 'We built our data transformation engine with the Wasm runtime',
    __v: 0,
  },
  {
    _id: '61fff30bc17607f336d1e49f',
    isDeleted: false,
    objectID: '30221632',
    tags: ['comment', 'author_dysoco', 'story_30206989'],
    createdAt: '2022-02-05T14:54:33.000Z',
    url: 'https://reviewbunny.app/blog/dont-make-me-think-or-why-i-switched-to-rails-from-javascript-spas',
    author: 'dysoco',
    title:
      "Don't make me think, or why I switched to Rails from JavaScript SPAs",
    __v: 0,
  },
  {
    _id: '61fff30bc17607f336d1e49d',
    isDeleted: false,
    objectID: '30222036',
    tags: ['comment', 'author_throwaway894345', 'story_30201969'],
    createdAt: '2022-02-05T15:32:35.000Z',
    url: 'https://benhoyt.com/writings/go-version-performance/',
    author: 'throwaway894345',
    title: 'Go performance from version 1.2 to 1.18',
    __v: 0,
  },
  {
    _id: '61fff30bc17607f336d1e4a1',
    isDeleted: false,
    objectID: '30215982',
    tags: ['comment', 'author_javajosh', 'story_30189609'],
    createdAt: '2022-02-05T01:15:38.000Z',
    url: 'https://binary-tools.net/summit.html',
    author: 'javajosh',
    title: 'Binary Tools Summit 2022',
    __v: 0,
  },
  {
    _id: '61fff30bc17607f336d1e49d',
    isDeleted: false,
    objectID: '30222036',
    tags: ['comment', 'author_throwaway894345', 'story_30201969'],
    createdAt: '2022-02-05T15:32:35.000Z',
    url: 'https://benhoyt.com/writings/go-version-performance/',
    author: 'throwaway894345',
    title: 'Go performance from version 1.2 to 1.18',
    __v: 0,
  },
  {
    _id: '61fff30bc17607f336d1e4b9',
    isDeleted: false,
    objectID: '30207045',
    tags: ['comment', 'author_Abishek_Muthian', 'story_30201969'],
    createdAt: '2022-02-04T14:22:36.000Z',
    url: 'https://benhoyt.com/writings/go-version-performance/',
    author: 'Abishek_Muthian',
    title: 'Go performance from version 1.2 to 1.18',
    __v: 0,
  },
  {
    _id: '61fff30bc17607f336d1e4a1',
    isDeleted: false,
    objectID: '30215982',
    tags: ['comment', 'author_javajosh', 'story_30189609'],
    createdAt: '2022-02-05T01:15:38.000Z',
    url: 'https://binary-tools.net/summit.html',
    author: 'javajosh',
    title: 'Binary Tools Summit 2022',
    __v: 0,
  },
  {
    _id: '61fff30bc17607f336d1e4ab',
    isDeleted: false,
    objectID: '30210225',
    tags: ['comment', 'author_followtherhythm', 'story_30209706'],
    createdAt: '2022-02-04T17:59:07.000Z',
    url: 'https://redpanda.com/blog/data-transformation-engine-with-wasm-runtime/',
    author: 'followtherhythm',
    title:
      'Redpanda: We built our data transformation engine with the WASM runtime',
    __v: 0,
  },
  {
    _id: '620000133f4d1623294b8bf4',
    isDeleted: false,
    objectID: '30234257',
    tags: ['comment', 'author_l00sed', 'story_30232344'],
    createdAt: '2022-02-06T16:54:34.000Z',
    url: 'https://github.com/CadQuery/cadquery',
    author: 'l00sed',
    title:
      'CadQuery –- A Python parametric CAD scripting framework based on OCCT',
    __v: 0,
  },
];
export const toBeCreateNews = {
  objectID: '30213770',
  tags: ['comment', 'author_password4321', 'story_30206989'],
  createdAt: '2022-02-04T21:47:35.000Z',
  url: 'https://reviewbunny.app/blog/dont-make-me-think-or-why-i-switched-to-rails-from-javascript-spas',
  author: 'password4321',
  title: "Don't make me think, or why I switched to Rails from JavaScript SPAs",
};
